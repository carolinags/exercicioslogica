package exerciciosLogica;

import java.util.Scanner;

public class Exercicio20 {

	public void run() {
		/* 20. Escreva um programa que leia um valor real correspondente a uma medida em metros, convertendo o valor dado em p�s
(1 metro = 3.315 p�s), exibindo os valores dado e convertido. Caso o usu�rio forne�a um valor negativo, deve ser exibida
uma mensagem e a opera��o de convers�o n�o deve ser efetuada.  */
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite um valor em metros para conversao: ");
		int m = input.nextInt();
		input.close();
		if(m<0) {
			System.out.print("Valor invalido");
		}else {
			System.out.print("Valor inserido: "+ m +" metros.\nValor convertido: "+ m*3.315 + " pés");
		}		
		
		
	}

}
