package exerciciosLogica;

import java.util.Scanner;

public class Exercicio12 {

	public void run() {
		/* 12. Escreva um programa capaz de calcular o pre�o total a ser pago por uma compra de copos pl�sticos. O usu�rio deve
fornecer o n�mero de copos a serem comprados e o programa deve calcular o pre�o total a ser cobrado, exibindo-o.
Observe que: se o n�mero de copo � inferior ou igual a 100, o pre�o por copo � R$0.05; se o n�mero de copos est� entre
101 e 500, o pre�o por copo � R$0.04; finalmente se o n�mero de copos � superior a 500 o pre�o por copo � R$0.035.  */
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite o n�mero de copos a serem comprados: ");
		int nCopos = input.nextInt();
		
		
		
		if ( nCopos <= 100) { 
			System.out.print("Ordem: "+ nCopos *0.05);
		} else if( nCopos > 100 &&  nCopos <= 500) {
			System.out.print("Ordem: "+ nCopos *0.04);
		}else if( nCopos > 500) {
			System.out.print("Valor total: R$ ");
			System.out.format("%.2f", nCopos *0.035);
		}
		
		
	}

}
