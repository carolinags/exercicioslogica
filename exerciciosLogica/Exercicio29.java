package exerciciosLogica;

import java.util.Scanner;

public class Exercicio29 {

	public void run() {
		/* 29. Faça um programa que exiba a tabela verdade da operação OU-Lógico para todas as combinações de três variáveis
lógicas A, B e C. Os resultados da tabela devem ser exibidos como F ou V. */
		Scanner input = new Scanner(System.in);
		
		String a[] = {"V","V","V","V","F","F","F","F"};
		String b[] = {"V","V","F","F","V","V","F","F"};
		String c[] = {"V","F","V","F","V","F","V","F"};
		int i;
		System.out.println("A B C"); 
		for(i=0;i<a.length;i++){
				System.out.print(a[i] + " " + b[i] + " "+ c[i] + " "); 
				if(a[i]=="V" || b[i]=="V" || c[i]=="V") {
					System.out.println("= V"); 
				}else {
					System.out.println("= F");
				}
		}
	}

}
