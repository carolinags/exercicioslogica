package exerciciosLogica;

import java.util.Scanner;

public class Exercicio25 {

	public void run() {
		/* 25. Faça um programa que calcule a soma de N números quaisquer fornecidos pelo usuário. */
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite quantos valores serão somados: ");
		int n = input.nextInt();
		int cont, soma = 0;
		for(cont=0;cont<n;cont++) {
			System.out.print("Digite o "+ (cont+1) + "º valor para soma: ");
			int num = input.nextInt();
			soma +=num;
		}
		System.out.print("Resultado: "+ soma);		
		
	}

}
