package exerciciosLogica;

import java.util.Scanner;

public class Exercicio13 {

	public void run() {
		/* Dados 3 valores reais representando os lados de um poss�vel tri�ngulo, verifique se os mesmos formam um tri�ngulo (a
medida de qualquer um de seus lados deve ser menor que a soma das medidas dos lados restantes). Caso seja formado um
tri�ngulo, descubra tamb�m se este tri�ngulo � escaleno (tr�s lados diferentes), is�sceles (dois lados iguais) ou eq�il�tero
(tr�s lados iguais).  */
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite um valor para o lado A do triangulo: ");
		int a = input.nextInt();
		System.out.print("Digite um valor para o lado B do triangulo: ");
		int b = input.nextInt();
		System.out.print("Digite um valor para o lado C do triangulo: ");
		int c = input.nextInt();
		
		String  tipoTriagulo = ""; 
		if(a==b && b==c) {
			tipoTriagulo = "escaleno";
		}else if((a==b || b==c) && (a !=c || b != a) ) {
			tipoTriagulo = "is�celes";
		}else if(a!=b && b!=c) {
			tipoTriagulo = "escaleno";
		}
				
		if(a <=0 || b <=0 || c<= 0) {
			System.out.print("Valores negativos n�o constituem um triangulo");
		} else if ( (a>(b-c)) && (a<(b+c))) { 
				System.out.print("Triangulo valido e " + tipoTriagulo);
		} else if ( (b>(a-c)) && (b<(a+c))) { 
			System.out.print("Triangulo valido e " + tipoTriagulo);
		} else if ( (c>(b-a)) && (a<(b+a))) { 
			System.out.print("Triangulo valido e " + tipoTriagulo);
		} 
		
	}	
	

}
