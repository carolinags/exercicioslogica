package exerciciosLogica;

import java.util.Scanner;

public class Exercicio27 {

	public void run() {
		/* 27. Calcule a média de Notas de alunos. O programa deve parar de ler valores quando for fornecido um valor negativo como
nota. */
		Scanner input = new Scanner(System.in);
		
		int cont=1, flag = 1, media=0;
		System.out.print("Digite um numero negativo para parar\n");
		while(flag==1) {
			System.out.print("Digite a " + cont + "ª nota: ");
			int nota = input.nextInt();
			if(nota>=0) {
				media += nota;
				cont++;
			}else {
				flag=0;
			}
		}
		System.out.print("Média final: " + (media/(cont-1)));
	}

}
