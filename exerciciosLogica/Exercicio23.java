package exerciciosLogica;

import java.util.Scanner;

public class Exercicio23 {

	public void run() {
		/* 23. Escreva um programa que receba as seguintes informações: um valor real indicando capital inicial PV, um valor real que
corresponde a taxa de juros da aplicação J e um número inteiro de períodos da aplicação N. O programa deve retornar o
capital futuro FV dado pela relação abaixo:
FV = PV * ( 1 + J ) N */
		Scanner input = new Scanner(System.in);
		
		System.out.print("Informe um valor real indicando capital inicial: ");
		double pv = input.nextDouble();
		System.out.print("Informe m valor real que corresponde a taxa de juros da aplicação: ");
		double j = input.nextDouble();
		System.out.print("Informe um número inteiro de períodos da aplicação: ");
		int n = input.nextInt();
		double fv = pv * Math.pow((1+j), n);
		System.out.print("a posição inicial do movel é: "+ fv);		
		
	}

}
