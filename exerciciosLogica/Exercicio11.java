package exerciciosLogica;

import java.util.Scanner;

public class Exercicio11 {

	public void run() {
		/* 11. Escreva um programa que leia dois n�meros inteiros A e B quaisquer indicando se A � m�ltiplo de B ou se B � m�ltiplo de A. */
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite um valor para A: ");
		int a = input.nextInt();
		System.out.print("Digite um valor para B: ");
		int b = input.nextInt();
		
		
		if ( (a%b == 0) ) { 
			System.out.print("A � multiplo de B");
		} 
		if( (b%a == 0)) { 
			System.out.print("B � multiplo de A");
		} 
		if((a%b != 0) && (b%a != 0)) { 
			System.out.print("Nenhum dos valores � m�tiplo do outro");
		} 
		
		
	}

}
