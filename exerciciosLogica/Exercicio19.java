package exerciciosLogica;

import java.util.Scanner;

public class Exercicio19 {

	public void run() {
		/* Escreva um programa que leia um n�mero real entre 0.00 e 100.00 e o exiba por extenso como se fosse uma quantia em
dinheiro, por exemplo: 1.00 -> "um real", .12.73 -> "doze reais e setenta e tr�s centavos".  */
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite um valor: ");
		double number = input.nextDouble();
		
	    String und[] = {"",  "um", "dois", "tr�s", "quatro", "cinco",
	                  "seis", "sete", "oito", "nove", "dez", "onze",
	                  "doze", "treze", "quatorze", "quinze", "dezesseis",
	                  "dezessete", "dezoito", "dezenove"};
	    
	    String dez[] = {"", "", "vinte", "trinta", "quarenta", "cinquenta",
	                  "sessenta", "setenta", "oitenta", "noventa"};
	    
	    String type[]= {" real", " reais", " centavos"};
	    
	    
	    // reais 
	    int num = (int) number;
	    if(num < 0 || num >100) {
			   System.out.print("Numero invalido");
		   }else if(num==0) {
			   System.out.print(num + ": zero"+ type[1]);
		   }else if(num==1) {
			   System.out.print(num + ": "+ und[num]+ type[0]);
		   }else if(num>0 && num<20) {
			   System.out.print(num + ": " + und[num] + type[1]);
		   }else if(num >=20 && num <=99) {			   
			   
			   int numund=num/10;
			   int numdec=num%10;
			   
			   System.out.print(num + ": " + dez[numund]);
			  
			   if(numdec!=0) {
				   System.out.print(" e " + und[numdec] + type[1]);
			   }else {
				   System.out.print(type[1]);
			   }
			   
		   }else {
			   System.out.print(num + ": cem"+ type[1]);
		   }
	    
	    //centavos
	    
	    	String subnumber =String.format("%.2f",number);
	    	subnumber = String.valueOf(subnumber);
	    	subnumber = subnumber.substring(subnumber.indexOf(".")).substring(1);
			int decimals = Integer.parseInt(subnumber);
			
			if(decimals > 0 && decimals <20) {
				  System.out.print(" e " + und[decimals] + type[2]);				
			}else if(decimals>=20 && decimals <100) {
					
				int numund=decimals%10;
				int numdec=decimals/10;
					
				   System.out.print(" e " +dez[numdec]);
				  
				   if(numund!=0) {
					   System.out.print(" e " + und[numund] + type[2]);
				   }else {
					   System.out.print(type[2]);
				   }
			}
	}

}
