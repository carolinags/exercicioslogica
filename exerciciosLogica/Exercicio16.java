package exerciciosLogica;

import java.util.Scanner;

public class Exercicio16 {

	public void run() {
		/* 16. Preparar um programa para ler as medidas da base e da altura de um tri�ngulo, calculando e imprimindo sua �rea,
sabendo que o c�lculo da �rea � dado por: */
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Insira o valor da base do triangulo: ");
		int base = input.nextInt();
		System.out.print("Insira o valor da altura do triangulo: ");
		int altura = input.nextInt();

		System.out.print("A �rea do triangulo � de "+ ((base*altura)/2));
		
		
		
	}

}
