package exerciciosLogica;

import java.util.Scanner;

public class Exercicio14 {

	public void run() {
		/* 14. Escreva um programa que leia 3 notas (valores reais), calculando e exibindo sua m�dia aritm�tica. Imprima tamb�m
"Aprovado" se a m�dia for maior que 7, "Reprovado" se for menor que 3 e "Exame" se estiver entre 3 e 7.  */
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite a primeira nota: ");
		double a = input.nextDouble();
		System.out.print("Digite a segunda nota: ");
		double b = input.nextDouble();
		System.out.print("Digite a terceira nota: ");
		double c = input.nextDouble();
		
		double media = Math.round((a+b+c)/3);
		
		if ( media>=7) { 
			System.out.print("Nota final: "+ media + ". Aprovado!");
		} else if ( media<=3) { 
			System.out.print("Nota final: "+ media + ". Reprovado!");
		} else if ( media>3 && media<7) { 
			System.out.print("Nota final: "+ media + ". Exame!");
		}	
				
	}

}
