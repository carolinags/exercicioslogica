package exerciciosLogica;

import java.util.Scanner;

public class Exercicio30 {

	public void run() {
		/* 30. Escreva um programa que calcule o valor de y = f(x) = a*x + b num intervalo de valores de x, sendo dados pelo usuário
os coeficientes a e b, bem como os valores do limite inferior e superior do intervalo de x bem como o valor do incremento
de x. O programa deve exibir resultados como um tabela onde constam os valores de x e y=f(x) lado a lado. */
		Scanner input = new Scanner(System.in);
		
		
		System.out.print("Digite o coeficientes a: ");
		int a= input.nextInt();		
		System.out.print("Digite o coeficientes b: ");
		int b= input.nextInt();
		System.out.print("Digite o valor do limite inferior de x: ");
		int lix= input.nextInt();
		System.out.print("Digite o valor do limite superior de x: ");
		int lsx= input.nextInt();		
		System.out.print("Digite o valor do incremento de x: ");
		int Ix= input.nextInt();		
		int resul = 0;	
		
		int i = (int)lix;
		
		while(i < lsx) {
			
			resul = (a * i) + b;			
			System.out.println("Se x: |"+ i + "|, y é:" + " |" + resul + "| f(" + i + ")");
			i = i + Ix;			
		}
		
	}

}
