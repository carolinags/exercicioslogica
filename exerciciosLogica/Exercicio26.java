package exerciciosLogica;

import java.util.Scanner;

public class Exercicio26 {

	public void run() {
		/* 26. Escreva um programa que calcule a soma de N números quaisquer fornecidos pelo usuário. O programa deve parar de ler
e, portanto somar os valores quando for introduzido o número 0 (zero). */
Scanner input = new Scanner(System.in);
		
		System.out.print("Digite os valores para soma ");
		System.out.print("\nE 0 para finalizar \n");
		int flag = 1, soma = 0;
		while(flag==1) {
			System.out.print("+ ");
			int num = input.nextInt();
			if(num !=0) {
				soma += num;
			}else {
				System.out.print("Resultado: "+ soma);
				flag = 0;
				
			}
		}		
	}
}
