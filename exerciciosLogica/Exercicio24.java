package exerciciosLogica;

import java.util.Scanner;

public class Exercicio24 {

	public void run() {
		/* 24. Implementar três programas para exibir a tabuada de um número inteiro entre 1 e 20 dado pelo usuário.
			a)Usando enquanto (while)
			b)Usando faça-enquanto (do-while)
			c)Usando para (for)
		Obs. Caso o valor dado seja inválido o programa deve apenas exibir uma mensagem de aviso. */
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Informe um valor inteiro entra 1 e 20: ");
		int num = input.nextInt();
		if(num<=20 && num >=0) {
			System.out.print("Escolha uma forma de processamento\n"
					+ "			a)Usando enquanto (while)\n"
					+ "			b)Usando faça-enquanto (do-while)\n"
					+ "			c)Usando para (for)\n: ");
			char opt = input.next().charAt(0);
			int cont = 0;
			switch(opt) {
				case 'a':
					while(cont<=10) {
						System.out.print("\n" +num + " * " + cont + " = " + num*cont);
						cont++;
					}
					break;
				case 'b':
					do {
						System.out.print("\n" + num + " * " + cont + " = " + num*cont);
						cont++;
					}while(cont<=10);
					break;
				case 'c':
					for(cont=0;cont<=10;cont++) {
						System.out.print("\n" +num + " * " + cont + " = " + num*cont);
					}
					break;
				default:
					System.out.print("Opção invalida");
					break;
			}
		}else {
			System.out.print("Número invalido");
		}
		
	}

}
