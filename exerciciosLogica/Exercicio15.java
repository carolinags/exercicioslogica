package exerciciosLogica;

import java.util.Scanner;

public class Exercicio15 {

	public void run() {
		/* 15. Escreva um programa que realize as seguintes tarefas:
			a) Efetue a leitura de dois valores inteiros, os quais ser�o denominados x e y;
			b) Realize a soma dos valores x e y, exibindo seu resultado;
			c) Realize o produto dos valores x e y exibindo seu resultado;
			d) Compare os valores x e y, indicando se "X > Y", "X = Y" e "X < Y" para os casos correspondentes.  */
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Insira o primerio valor: ");
		int x = input.nextInt();
		System.out.print("Insira o segundo valor: ");
		int y = input.nextInt();
		
		System.out.println("x + y = "+ (x+y));
		System.out.println("x * y = "+ (x*y));
		if(x<y) {
			System.out.println( x + " < " + y);
		}else if(x>y) {
			System.out.println( x + " > " + y);
		}else if(x==y) {
			System.out.println( x + " = " + y);
		}
				
		
	}

}
