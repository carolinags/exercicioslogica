package exerciciosLogica;

import java.util.Scanner;

public class Exercicio21 {

	public void run() {
		/* 21. Faça um algoritmo que, lendo 3 números correspondentes aos coeficientes a, b, e c de uma equação do 2º grau, calcule
seu DELTA e também as raízes desta equação, imprimindo seus valores. */
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Segundo a equação ax² + bx + c = 0\n");
		System.out.print("Digite um valor para a: ");
		double a = input.nextDouble();
		System.out.print("Digite um valor para b: ");
		double b = input.nextDouble();
		System.out.print("Digite um valor para c: ");
		double c = input.nextDouble();
		double delta = Math.pow(b, 2) + 4*a*c;
		System.out.print("O delta da equação é: "+ delta);
		delta = Math.sqrt(delta);
		System.out.print("\nE suas raizes sao: "+ (-b+delta)/(2*a) + " e " + (-b-delta)/(2*a));
		
	}

}
