package exerciciosLogica;

import java.util.Scanner;

public class Exercicio22 {

	public void run() {
		/* 22. Considerando um objeto móvel em movimento uniformemente variado, escreva um programa que receba as seguintes
informações: um valor real indicando posição inicial do móvel P0, um valor real que corresponde a velocidade do móvel
V, um outro valor real A correspondente a aceleração do móvel e um número inteiro correspondente ao tempo decorrido
T. O programa deve calcular a posição final PF do móvel, dado pela relação abaixo:
PF = P0 + V * T + (A * T 2 ) / 2 */
		Scanner input = new Scanner(System.in);
		
		System.out.print("Informe a posição inicial do movel: ");
		double p0 = input.nextDouble();
		System.out.print("Informe a velocidade do movel: ");
		double v = input.nextDouble();
		System.out.print("Informe a aceleração do movel: ");
		double a = input.nextDouble();
		System.out.print("Informe o tempo decorrido: ");
		int t = input.nextInt();
		double pf = (p0 + (v*t) + Math.pow(t, 2)*a)/2;
		System.out.print("a posição inicial do movel é: "+ pf);
		
	}

}
