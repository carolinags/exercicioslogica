package exerciciosLogica;

import java.util.Scanner;

public class Exercicio18 {

	public void run() {
		/* Escreva um programa que leia um n�mero inteiro entre 0 e 100 e o exiba por extenso, por exemplos: 16 -> "dezesseis",
34 -> "trinta e quatro", etc.. */
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Digite um valor: ");
		int num = input.nextInt();
		
	    String und[] = {"",  "um", "dois", "tr�s", "quatro", "cinco",
	                  "seis", "sete", "oito", "nove", "dez", "onze",
	                  "doze", "treze", "quatorze", "quinze", "dezesseis",
	                  "dezessete", "dezoito", "dezenove"};
	    
	    String dez[] = {"", "", "vinte", "trinta", "quarenta", "cinquenta",
	                  "sessenta", "setenta", "oitenta", "noventa"};
	    if(num < 0 || num >100) {
			   System.out.print("Numero invalido");
		   } else if(num==0) {
			   System.out.print(num + ": zero");
		   }else if(num>0 && num<20) {
			   System.out.print(num + ": " + und[num]);
		   }else if(num >=20 && num <=99) {
			   String str=String.valueOf(num);
			   String[] posicao = str.split("");
			   int u=Integer.parseInt(posicao[0]);
			   int d=Integer.parseInt(posicao[1]);
			   System.out.print(num + ": " + dez[u]);
			   if(d!=0) {
				   System.out.print(" e " + und[d]);
			   }
		   }else {
			   System.out.print(num + ": cem");
		   }
	}

}
